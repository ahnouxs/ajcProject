package com.training.maven.ajc;

import junit.framework.TestCase;

public class TestAddition extends TestCase {
	Addition myAdd;

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		 myAdd = new Addition();
	}
	
	public void testUneAddition() {
		myAdd.setNum1(8);
		myAdd.setNum2(2);
		assertEquals(10,myAdd.addTwoNumber());
		
	}
	
	public void testUneAdditionErreur() {
		myAdd.setNum1(8);
		myAdd.setNum2(2);
		assertNotSame(9,myAdd.addTwoNumber());
		
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}
	
	

}
